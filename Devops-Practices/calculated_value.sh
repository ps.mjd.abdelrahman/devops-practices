#!/bin/bash

# Get current date in the format YYMMDD
date=$(date +‘%y%m%d’)

# Get the first eight digits of the commit hash
commit_sha=$(git rev-parse --short=8 HEAD)

# Construct the version value
version=“$date-$commit_sha”

echo “$version”
