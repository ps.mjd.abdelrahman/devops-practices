#!/bin/bash

COMMIT_HASH=$(git rev-parse --short=8 HEAD)
COMMIT_DATE=$(git log -1 --format=%cd --date=format:‘%y%m%d’)

echo “$COMMIT_DATE-$COMMIT_HASH”
