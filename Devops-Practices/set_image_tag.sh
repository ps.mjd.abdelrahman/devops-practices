#!/bin/bash

# Get the current date in the format YYMMDD
date=$(date +'%y%m%d')

# Get the first eight characters of the Git commit hash
commit_hash=$(git rev-parse --short HEAD)

# Combine date and commit hash to form the image tag
image_tag="${date}-${commit_hash}"

# Set the image tag in the docker-compose.yml file
sed -i "s|psmjd/dockerpractice:latest|psmjd/dockerpractice:${image_tag}|g" docker-compose.yml
